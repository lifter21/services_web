import { Component } from '@angular/core';
import {Http, Response} from '@angular/http';


@Component({
  selector: 'app-post-message',
  templateUrl: './post-message.component.html',
  styleUrls: ['./post-message.component.css']
})
export class PostMessageComponent {
  data: Object;
  loading: boolean;

  public recipients: Object[] = [
    {id: 1, name: 'Alex'},
    {id: 2, name: 'Simon'},
    {id: 3, name: 'Veronica'},
    {id: 4, name: 'Andy'},
    {id: 5, name: 'Zak'},
    {id: 6, name: 'Lucy'},
    {id: 7, name: 'Clark'},
  ];
  public selectedOptions;

  constructor(private http: Http) {
  }

  makeRequest(): void {
    this.loading = true;
    this.http.request('http://jsonplaceholder.typicode.com/posts/1')
      .subscribe((res: Response) => {
        this.data = res.json();
        this.loading = false;
      });
  }

  sendMessage(messageRecipients: HTMLInputElement, messageText: HTMLInputElement): void {
    // console.log(messageRecipients.value, messageText.value)
  }

  // ngOnInit() {
  // }

}
